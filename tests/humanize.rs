extern crate chrono;
extern crate chrono_humanize;

#[cfg(test)]
mod duration {
    use chrono::Duration;
    use chrono_humanize::Humanize;

    #[test]
    fn now() {
        let english = Duration::zero().humanize();
        assert_eq!("现在", english);
    }

    #[test]
    fn plus_5s() {
        let english = Duration::seconds(5).humanize();
        assert_eq!("现在", english);
    }

    #[test]
    fn minus_5s() {
        let english = Duration::seconds(-5).humanize();
        assert_eq!("现在", english);
    }

    #[test]
    fn plus_15s() {
        let english: String = Duration::seconds(15).humanize();
        assert_eq!("剩余 15 秒", english);
    }

    #[test]
    fn minus_15s() {
        let english = Duration::seconds(-15).humanize();
        assert_eq!("15 秒前", english);
    }

    #[test]
    fn plus_95s() {
        let english = Duration::seconds(95).humanize();
        assert_eq!("剩余 2 分", english);
    }

    #[test]
    fn minus_95s() {
        let english = Duration::seconds(-95).humanize();
        assert_eq!("2 分前", english);
    }

    #[test]
    fn plus_125s() {
        let english = Duration::seconds(125).humanize();
        assert_eq!("剩余 2 分", english);
    }

    #[test]
    fn minus_125s() {
        let english = Duration::seconds(-125).humanize();
        assert_eq!("2 分前", english);
    }

    #[test]
    fn plus_31m() {
        let english = Duration::minutes(31).humanize();
        assert_eq!("剩余 31 分", english);
    }

    #[test]
    fn minus_31m() {
        let english = Duration::minutes(-31).humanize();
        assert_eq!("31 分前", english);
    }

    #[test]
    fn plus_45m() {
        let english = Duration::minutes(45).humanize();
        assert_eq!("剩余 45 分", english);
    }

    #[test]
    fn minus_45m() {
        let english = Duration::minutes(-45).humanize();
        assert_eq!("45 分前", english);
    }

    #[test]
    fn plus_46m() {
        let english = Duration::minutes(46).humanize();
        assert_eq!("剩余 1 小时", english);
    }

    #[test]
    fn minus_46m() {
        let english = Duration::minutes(-46).humanize();
        assert_eq!("1 小时前", english);
    }

    #[test]
    fn plus_1h() {
        let english = Duration::hours(1).humanize();
        assert_eq!("剩余 1 小时", english);
    }

    #[test]
    fn minus_1h() {
        let english = Duration::hours(-1).humanize();
        assert_eq!("1 小时前", english);
    }

    #[test]
    fn plus_12h() {
        let english = Duration::hours(12).humanize();
        assert_eq!("剩余 12 小时", english);
    }

    #[test]
    fn minus_12h() {
        let english = Duration::hours(-12).humanize();
        assert_eq!("12 小时前", english);
    }

    #[test]
    fn plus_23h() {
        let english = Duration::hours(23).humanize();
        assert_eq!("剩余 1 天", english);
    }

    #[test]
    fn minus_23h() {
        let english = Duration::hours(-23).humanize();
        assert_eq!("1 天前", english);
    }

    #[test]
    fn plus_26h() {
        let english = Duration::hours(26).humanize();
        assert_eq!("剩余 1 天", english);
    }

    #[test]
    fn minus_26h() {
        let english = Duration::hours(-26).humanize();
        assert_eq!("1 天前", english);
    }

    #[test]
    fn plus_1d() {
        let english = Duration::days(1).humanize();
        assert_eq!("剩余 1 天", english);
    }

    #[test]
    fn minus_1d() {
        let english = Duration::days(-1).humanize();
        assert_eq!("1 天前", english);
    }

    #[test]
    fn plus_2d() {
        let english = Duration::days(2).humanize();
        assert_eq!("剩余 2 天", english);
    }

    #[test]
    fn minus_2d() {
        let english = Duration::days(-2).humanize();
        assert_eq!("2 天前", english);
    }

    #[test]
    fn plus_6d_13h() {
        let english = (Duration::days(6) + Duration::hours(13)).humanize();
        assert_eq!("剩余 1 周", english);
    }

    #[test]
    fn minus_6d_13h() {
        let english = (Duration::days(-6) + Duration::hours(-13)).humanize();
        assert_eq!("1 周前", english);
    }

    #[test]
    fn plus_7d() {
        let english = Duration::days(7).humanize();
        assert_eq!("剩余 1 周", english);
    }

    #[test]
    fn minus_7d() {
        let english = Duration::days(-7).humanize();
        assert_eq!("1 周前", english);
    }

    #[test]
    fn plus_10d() {
        let english = Duration::days(10).humanize();
        assert_eq!("剩余 1 周", english);
    }

    #[test]
    fn minus_10d() {
        let english = Duration::days(-10).humanize();
        assert_eq!("1 周前", english);
    }

    #[test]
    fn plus_11d() {
        let english = Duration::days(11).humanize();
        assert_eq!("剩余 2 周", english);
    }

    #[test]
    fn minus_11d() {
        let english = Duration::days(-11).humanize();
        assert_eq!("2 周前", english);
    }

    #[test]
    fn plus_4w() {
        let english = Duration::weeks(4).humanize();
        assert_eq!("剩余 4 周", english);
    }

    #[test]
    fn minus_4w() {
        let english = Duration::weeks(-4).humanize();
        assert_eq!("4 周前", english);
    }

    #[test]
    fn plus_30d() {
        let english = Duration::days(30).humanize();
        assert_eq!("剩余 1 个月", english);
    }

    #[test]
    fn minus_30d() {
        let english = Duration::days(-30).humanize();
        assert_eq!("1 个月前", english);
    }

    #[test]
    fn plus_45d() {
        let english = Duration::days(45).humanize();
        assert_eq!("剩余 1 个月", english);
    }

    #[test]
    fn minus_45d() {
        let english = Duration::days(-45).humanize();
        assert_eq!("1 个月前", english);
    }

    #[test]
    fn plus_46d() {
        let english = Duration::days(46).humanize();
        assert_eq!("剩余 2 个月", english);
    }

    #[test]
    fn minus_46d() {
        let english = Duration::days(-46).humanize();
        assert_eq!("2 个月前", english);
    }

    #[test]
    fn plus_24w() {
        let english = Duration::weeks(24).humanize();
        assert_eq!("剩余 5 个月", english);
    }

    #[test]
    fn minus_24w() {
        let english = Duration::weeks(-24).humanize();
        assert_eq!("5 个月前", english);
    }

    #[test]
    fn plus_26w() {
        let english = Duration::weeks(26).humanize();
        assert_eq!("剩余 6 个月", english);
    }

    #[test]
    fn minus_26w() {
        let english = Duration::weeks(-26).humanize();
        assert_eq!("6 个月前", english);
    }

    #[test]
    fn plus_50w() {
        let english = Duration::weeks(50).humanize();
        assert_eq!("剩余 1 年", english);
    }

    #[test]
    fn minus_50w() {
        let english = Duration::weeks(-50).humanize();
        assert_eq!("1 年前", english);
    }

    #[test]
    fn plus_100w() {
        let english = Duration::weeks(100).humanize();
        assert_eq!("剩余 2 年", english);
    }

    #[test]
    fn minus_100w() {
        let english = Duration::weeks(-100).humanize();
        assert_eq!("2 年前", english);
    }

    #[test]
    fn plus_120w() {
        let english = Duration::weeks(120).humanize();
        assert_eq!("剩余 2 年", english);
    }

    #[test]
    fn minus_120w() {
        let english = Duration::weeks(-120).humanize();
        assert_eq!("2 年前", english);
    }

    #[test]
    fn plus_200w() {
        let english = Duration::weeks(200).humanize();
        assert_eq!("剩余 3 年", english);
    }

    #[test]
    fn minus_200w() {
        let english = Duration::weeks(-200).humanize();
        assert_eq!("3 年前", english);
    }
}

#[cfg(test)]
mod utc {
    use chrono::Utc;
    use chrono_humanize::Humanize;

    #[test]
    fn now() {
        let english: String = Utc::now().humanize();
        assert_eq!("现在", english);
    }
}

#[cfg(test)]
mod local {
    use chrono::{Duration, Local};
    use chrono_humanize::Humanize;

    #[test]
    fn now() {
        let english = Local::now().humanize();
        assert_eq!("现在", english);
    }

    #[test]
    fn minus_35d() {
        let past = Local::now() - Duration::days(35);
        let english = past.humanize();
        assert_eq!("1 个月前", english);
    }

    #[test]
    fn plus_35d() {
        let future = Local::now() + Duration::days(35);
        let english = future.humanize();
        assert_eq!("剩余 1 个月", english);
    }
}
