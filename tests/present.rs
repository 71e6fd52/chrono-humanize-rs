macro_rules! duration_test {
    ($($name:ident: $duration:expr, $rough:expr, $precise:expr,)+) => {
        $(#[test]
        fn $name() {
            let ht = HumanTime::from($duration);
            let rough = ht.to_text_zh(Accuracy::Rough, Tense::Present);
            let precise = ht.to_text_zh(Accuracy::Precise, Tense::Present);
            assert_eq!($rough, rough);
            assert_eq!($precise, precise);
        })+
    }
}

#[cfg(test)]
mod duration {
    use chrono::Duration;
    use chrono_humanize::{Accuracy, HumanTime, Tense};

    // test_name: Duration expression, "Rough text", "Precise text"
    duration_test! {
        现在: Duration::zero(), "现在", "0 秒",
        plus_1s: Duration::seconds(1), "现在", "1 秒",
        minus_1s: Duration::seconds(-1), "现在", "1 秒",
        plus_5s: Duration::seconds(5), "现在", "5 秒",
        minus_5s: Duration::seconds(-5), "现在", "5 秒",
        plus_15s: Duration::seconds(15), "15 秒", "15 秒",
        minus_15s: Duration::seconds(-15), "15 秒", "15 秒",
        plus_95s: Duration::seconds(95), "2 分", "1 分 35 秒",
        minus_95s: Duration::seconds(-95), "2 分", "1 分 35 秒",
        plus_125s: Duration::seconds(125), "2 分", "2 分 5 秒",
        minus_125s: Duration::seconds(-125), "2 分", "2 分 5 秒",
        plus_31m: Duration::minutes(31), "31 分", "31 分",
        minus_31m: Duration::minutes(-31), "31 分", "31 分",
        plus_45m: Duration::minutes(45), "45 分", "45 分",
        minus_45m: Duration::minutes(-45), "45 分", "45 分",
        plus_46m: Duration::minutes(46), "1 小时", "46 分",
        minus_46m: Duration::minutes(-46), "1 小时", "46 分",
        plus_1h: Duration::hours(1), "1 小时", "1 小时",
        minus_1h: Duration::hours(-1), "1 小时", "1 小时",
        plus_12h: Duration::hours(12), "12 小时", "12 小时",
        minus_12h: Duration::hours(-12), "12 小时", "12 小时",
        plus_23h: Duration::hours(23), "1 天", "23 小时",
        minus_23h: Duration::hours(-23), "1 天", "23 小时",
        plus_26h: Duration::hours(26), "1 天", "1 天 2 小时",
        minus_26h: Duration::hours(-26), "1 天", "1 天 2 小时",
        plus_1d: Duration::days(1), "1 天", "1 天",
        minus_1d: Duration::days(-1), "1 天", "1 天",
        plus_2d: Duration::days(2), "2 天", "2 天",
        minus_2d: Duration::days(-2), "2 天", "2 天",
        plus_6d_13h: Duration::days(6) + Duration::hours(13), "1 周", "6 天 13 小时",
        minus_6d_13h: Duration::days(-6) + Duration::hours(-13), "1 周", "6 天 13 小时",
        plus_7d: Duration::days(7), "1 周", "1 周",
        minus_7d: Duration::days(-7), "1 周", "1 周",
        plus_10d: Duration::days(10), "1 周", "1 周 3 天",
        minus_10d: Duration::days(-10), "1 周", "1 周 3 天",
        plus_11d: Duration::days(11), "2 周", "1 周 4 天",
        minus_11d: Duration::days(-11), "2 周", "1 周 4 天",
        plus_4w: Duration::weeks(4), "4 周", "4 周",
        minus_4w: Duration::weeks(-4), "4 周", "4 周",
        plus_30d: Duration::days(30), "1 个月", "1 个月",
        minus_30d: Duration::days(-30), "1 个月", "1 个月",
        plus_45d: Duration::days(45), "1 个月", "1 个月 2 周 1 天",
        minus_45d: Duration::days(-45), "1 个月", "1 个月 2 周 1 天",
        plus_46d: Duration::days(46), "2 个月", "1 个月 2 周 2 天",
        minus_46d: Duration::days(-46), "2 个月", "1 个月 2 周 2 天",
        plus_24w: Duration::weeks(24), "5 个月", "5 个月 2 周 4 天",
        minus_24w: Duration::weeks(-24), "5 个月", "5 个月 2 周 4 天",
        plus_26w: Duration::weeks(26), "6 个月", "6 个月 2 天",
        minus_26w: Duration::weeks(-26), "6 个月", "6 个月 2 天",
        plus_50w: Duration::weeks(50), "1 年", "11 个月 2 周 6 天",
        minus_50w: Duration::weeks(-50), "1 年", "11 个月 2 周 6 天",
        plus_100w: Duration::weeks(100), "2 年", "1 年 11 个月 5 天",
        minus_100w: Duration::weeks(-100), "2 年", "1 年 11 个月 5 天",
        plus_101w: Duration::weeks(101), "2 年", "1 年 11 个月 1 周 5 天",
        minus_101w: Duration::weeks(-101), "2 年", "1 年 11 个月 1 周 5 天",
        plus_120w: Duration::weeks(120), "2 年", "2 年 3 个月 2 周 6 天",
        minus_120w: Duration::weeks(-120), "2 年", "2 年 3 个月 2 周 6 天",
        plus_200w: Duration::weeks(200), "3 年", "3 年 10 个月 5 天",
        minus_200w: Duration::weeks(-200), "3 年", "3 年 10 个月 5 天",
    }
}

#[cfg(test)]
mod utc {
    use chrono::Utc;
    use chrono_humanize::{Accuracy, HumanTime, Tense};

    #[test]
    fn 现在() {
        let ht = HumanTime::from(Utc::now());
        let rough = ht.to_text_zh(Accuracy::Rough, Tense::Present);
        assert_eq!("现在", rough);
    }
}

#[cfg(test)]
mod local {
    use chrono::{Duration, Local};
    use chrono_humanize::{Accuracy, HumanTime, Tense};

    #[test]
    fn now() {
        let ht = HumanTime::from(Local::now());
        let rough = ht.to_text_zh(Accuracy::Rough, Tense::Present);
        assert_eq!("现在", rough);
    }

    #[test]
    fn minus_35d() {
        let past = Local::now() - Duration::days(35);
        let ht = HumanTime::from(past);
        let rough = ht.to_text_zh(Accuracy::Rough, Tense::Present);
        assert_eq!("1 个月", rough);
    }

    #[test]
    fn plus_35d() {
        let future = Local::now() + Duration::days(35);
        let ht = HumanTime::from(future);
        let rough = ht.to_text_zh(Accuracy::Rough, Tense::Present);
        assert_eq!("1 个月", rough);
    }
}
