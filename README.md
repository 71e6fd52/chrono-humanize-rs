## Chrono Humanize - Make your text time representation human appealing

Fork from https://gitlab.com/imp/chrono-humanize-rs.

Change it to Chinese.

## Quick Start

```rust
use chrono::{Local, Duration};
use chrono_humanize::HumanTime;

let dt = Local::now() + Duration::days(35);
let ht = HumanTime::from(dt);
let english = format!("{}", ht);
assert_eq!("in a month", english);
```
